psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "SELECT * FROM t100_nodes_link_parameters WHERE date_time > '2013-04-26 00:00:00'" -o t100_nodes_link_parameters.txt
psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "SELECT * FROM t100_nodes_node_parameters WHERE date_time > '2013-04-26 00:00:00'" -o t100_nodes_node_parameters.txt
psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "SELECT * FROM t100_nodes_properties1" -o t100_nodes_properties1.txt
psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "SELECT * FROM t100_nodes_trajectory" -o t100_nodes_trajectory.txt
psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "\
SELECT prop.mac_address, \
       prop.latitude, \
       prop.longitude, \
       unique_mac_prop.level, \
       unique_mac_prop.cost, \
       unique_mac_prop.battery_percentage, \
       unique_mac_prop.date_time \
  FROM t100_nodes_properties1 prop \
  NATURAL INNER JOIN 
  (SELECT mac_address, level, cost, battery_percentage, date_time, rank() \
    OVER (PARTITION BY mac_address ORDER BY date_time DESC) \
    FROM t100_nodes_node_parameters) AS unique_mac_prop \
  WHERE rank=1 \
" -o prop_uniqueparam_join.txt


psql -w -h db-00.vis.kaust.edu.sa -U sensors -A -F"," -c "SELECT * FROM t100_nodes_link_parameters WHERE date_time > '2013-04-26 00:00:00'" -o t100_nodes_link_parameters.csv --pset footer
psql -w -h db-00.vis.kaust.edu.sa -U sensors -A -F"," -c "SELECT * FROM t100_nodes_node_parameters WHERE date_time > '2013-04-26 00:00:00'" -o t100_nodes_node_parameters.csv --pset footer
psql -w -h db-00.vis.kaust.edu.sa -U sensors -A -F"," -c "SELECT * FROM t100_nodes_properties1" -o t100_nodes_properties1.csv --pset footer
psql -w -h db-00.vis.kaust.edu.sa -U sensors -A -F"," -c "SELECT * FROM t100_nodes_trajectory" -o t100_nodes_trajectory.csv --pset footer
psql -w -h db-00.vis.kaust.edu.sa -U sensors -A -F"," -c "\
SELECT prop.mac_address, \
       prop.latitude, \
       prop.longitude, \
       unique_mac_prop.level, \
       unique_mac_prop.cost, \
       unique_mac_prop.battery_percentage, \
       unique_mac_prop.date_time \
  FROM t100_nodes_properties1 prop \
  NATURAL INNER JOIN 
  (SELECT mac_address, level, cost, battery_percentage, date_time, rank() \
    OVER (PARTITION BY mac_address ORDER BY date_time DESC) \
    FROM t100_nodes_node_parameters) AS unique_mac_prop \
  WHERE rank=1 \
" -o prop_uniqueparam_join.csv --pset footer

perl rmws.pl t100_nodes_link_parameters.csv
perl rmws.pl t100_nodes_node_parameters.csv
perl rmws.pl t100_nodes_properties1.csv
perl rmws.pl t100_nodes_trajectory.csv
perl rmws.pl prop_uniqueparam_join.csv
