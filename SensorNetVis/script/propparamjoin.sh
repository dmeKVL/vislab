psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "\
SELECT prop.mac_address, \
       prop.latitude, \
       prop.longitude, \
       nparam.level, \
       nparam.cost, \
       nparam.battery_percentage, \
       nparam.date_time \
  FROM t100_nodes_properties1 prop\
  NATURAL INNER JOIN t100_nodes_node_parameters nparam \
"
