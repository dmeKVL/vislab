psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "\
  (SELECT mac_address, level, cost, battery_percentage, date_time, rank() \
    OVER (PARTITION BY mac_address ORDER BY date_time DESC) \
    FROM t100_nodes_node_parameters AS unique_mac_prop) AS prop_uniqueparam_join \
"

