psql -w -h db-00.vis.kaust.edu.sa -U sensors -c "\
SELECT prop.mac_address, \
       prop.latitude, \
       prop.longitude, \
       unique_mac_prop.level, \
       unique_mac_prop.cost, \
       unique_mac_prop.battery_percentage, \
       unique_mac_prop.date_time \
  FROM t100_nodes_properties1 prop \
  NATURAL INNER JOIN 
  (SELECT mac_address, level, cost, battery_percentage, date_time, rank() \
    OVER (PARTITION BY mac_address ORDER BY date_time DESC) \
    FROM t100_nodes_node_parameters) AS unique_mac_prop \
  WHERE rank=1 \
"
