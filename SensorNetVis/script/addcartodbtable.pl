#!/usr/bin/perl -w

use LWP::Simple;

my $num_args;
my $file;
my $sql_cmd;
my $cdb_url_prefix = "http://arshadsu.cartodb.com/api/v2/sql?q=";
my $cdb_url_suffix = "&api_key=5c7762f20fac7415c4487369ce05b5fa5b8063fd";
my $cdb_url;
my $header;
my @fields;
my $mac_address;
my $date_time;
my $row;

# check for correct number of args
$num_args = $#ARGV + 1;
#print $num_args."\n";
if ($num_args != 1) {
  print "Usage: perl addcartodbtable.pl <csv_filename>\n";
  exit;
}

# get the filename
$file = $ARGV[0];
open(INFILE, $file) or die "Cannot open $!";

# delete whatever is already in the CartoDB database
$sql_cmd = "DELETE FROM prop_uniqueparam_join_auto;";
$cdb_url = $cdb_url_prefix.$sql_cmd.$cdb_url_suffix;
print "$cdb_url\n";
getprint($cdb_url);
print "\n";

$header = <INFILE>;
chomp($header);

while (<INFILE>) {
  chomp;
  @fields = split(',', $_);
  $mac_address = shift(@fields);
  $date_time = pop(@fields);
  $row = "\'".$mac_address."\',".join(',',@fields).",\'".$date_time."\'";
  $sql_cmd = "INSERT INTO prop_uniqueparam_join_auto (".$header.") VALUES (".$row.");";
  $cdb_url = $cdb_url_prefix.$sql_cmd.$cdb_url_suffix;
  print "$cdb_url\n";
  getprint($cdb_url);
  print "\n";
}

close(INFILE);

# test whatever has been inserted into CartoDB database
$sql_cmd = "SELECT * FROM prop_uniqueparam_join_auto;";
$cdb_url = $cdb_url_prefix.$sql_cmd.$cdb_url_suffix;
print "$cdb_url\n";
getprint($cdb_url);
print "\n";



