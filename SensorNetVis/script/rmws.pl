#!/usr/bin/perl -w

use File::Basename;

my $num_args;
my $filepath;
my @path_comp;
my $file;
my $dir;
my $suffix;
my $dst_file;


# Check if file to process is given
$num_args = $#ARGV + 1;
#print $num_args."\n";
if ($num_args != 1) {
  print "Usage: perl rmwhitespace.pl <filename>\n";
}
else {
  $filepath = $ARGV[0];
  #print $filepath."\n";
  
  # Assume full path and separate into directory, filename and suffix
  ($file, $dir, $suffix) = fileparse($filepath, qr/\.[^.]*/);
  #print "file:".$file."\n";
  #print "dir:".$dir."\n";
  #print "suffix:".$suffix."\n";

  # Create temporary filename to write data to during whitespace removal
  $dst_file = "$dir$file.tmp";
  #print $dst_file."\n";

  # Open source file
  open(INFILE, $filepath) or die "Cannot open $!";

  # Open destination file
  open(OUTFILE, ">$dst_file") or die "Cannot open $!";

  while(<INFILE>) {
    chomp;
    #print "$_\n";
    @fields = split(',', $_);
    for (my $ii = 0; $ii < scalar(@fields); $ii++) {
      $fields[$ii] =~ s/^\s+//;
      $fields[$ii] =~ s/\s+$//;
    }
    $fieldsnows = join(',', @fields);
    print OUTFILE "$fieldsnows\n"
  }
  

  close(INFILE); 
  close(OUTFILE);

  $cmd = "mv $dst_file $filepath";
  #print "$cmd\n";
  system($cmd);
}

